import unittest
from grammar import pass_parser
from tagger import tokenize, load_model
from constants import (
    MDD2,
    GAW0,
    SOAD5,
    PREDD1,
    MDD20,
    MDD9,
    MDD3,
    SR3,
    MDD13,
    SR7,
    GAW2,
    GAW4,
    GAW5,
    GAW7,
    MDD6,
    MDD17,
    GAW0,
)


class TestNLP(unittest.TestCase):
    nlp = load_model()

    def test_nlp_case1(self):
        txt = "estoy triste"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertTrue(result == [MDD2])

    def test_nlp_case2(self):
        txt = "me siento triste y deprimido"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertTrue(result == [MDD2])

    def test_nlp_case3(self):
        txt = "me siento triste y deprimido, ansioso y agobiado"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertTrue(result == [MDD2, GAW0])

    def test_nlp_case4(self):
        txt = "no tengo fuerza"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [MDD9, GAW4])

    def test_nlp_case5(self):
        txt = "no tengo fuerza ni energia"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [MDD9, GAW4])

    def test_nlp_case6(self):
        txt = "no tengo fuerza ni energia y animos"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [MDD9, GAW4])

    def test_nlp_case7(self):
        txt = "me he sentido mal y deprimido"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD2])

    def test_nlp_case8(self):
        txt = "me he sentido mal, deprimido y ansioso"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertTrue(result == [MDD2, GAW0])

    def test_nlp_case9(self):
        txt = "yo siento poca motivacion"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [MDD9, GAW4])

    def test_nlp_case10(self):
        txt = "yo siento poca motivacion y energia"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [MDD9, GAW4])

    def test_nlp_case11(self):
        txt = "yo siento poca motivacion y energia"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [MDD9, GAW4])

    def test_nlp_case12(self):
        txt = "me siento mal"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertTrue(result == [MDD2])

    def test_nlp_case13(self):
        txt = "me siento mal y triste"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD2])

    def test_nlp_case14(self):
        txt = "me siento mal, triste y agobiado"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD2, GAW0])

    def test_nlp_case15(self):
        txt = "quiero matarme"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD20])

    def test_nlp_case16(self):
        txt = "me quiero matar"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD20])

    def test_nlp_case17(self):
        txt = "quiero lastimarme"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD20])

    def test_nlp_case18(self):
        txt = "nunca me he sentido amado"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [SOAD5])

    def test_nlp_case19(self):
        txt = "yo no me he sentido deseado"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [SOAD5])

    def test_nlp_case20(self):
        txt = "jamas me siento acompanado"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [SOAD5])

    def test_nlp_case21(self):
        txt = "siento que todos me desprecian"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [PREDD1, MDD17])

    def test_nlp_case22(self):
        txt = "me siento vacio"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD3])

    def test_nlp_case23(self):
        txt = "me siento sin proposito"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD3])

    def test_nlp_case24(self):
        txt = "estoy vacio"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD3])

    def test_nlp_case25(self):
        txt = "estoy solo"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [SR3])

    def test_nlp_case26(self):
        txt = "no he dormido"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [MDD13, GAW5])

    def test_nlp_case27(self):
        txt = "no he descansado"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertCountEqual(result, [MDD13, GAW5])

    def test_nlp_case29(self):
        txt = "me maltrataron de niño"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [SR7])

    def test_nlp_case30(self):
        txt = "fui abusado de niño"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [SR7])

    def test_nlp_case31(self):
        txt = "he sido abusado en mi infancia"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [SR7])

    def test_nlp_case32(self):
        txt = "he sido abusado en mi infancia"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [SR7])

    def test_nlp_case33(self):
        txt = "me irrito con facilidad"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [GAW2])

    def test_nlp_case34(self):
        txt = "me enojo con frecuencia"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [GAW2])

    def test_nlp_case35(self):
        txt = "mantengo enojado"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [GAW2])

    def test_nlp_case36(self):
        txt = "tengo poco apetito"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD6])

    def test_nlp_case37(self):
        txt = "yo como frecuentemente"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD6])

    def test_nlp_case38(self):
        txt = "tengo mucho apetito"
        tokens = tokenize(self.nlp, txt)
        result = pass_parser(tokens)
        self.assertListEqual(result, [MDD6])
