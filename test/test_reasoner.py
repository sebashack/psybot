import unittest
from symptoms import *
from constants import *
from reasoner import (
    init_reasoner,
    query_SEAD_intervention_options,
    query_SOAD_intervention_options,
    query_SEAD_intervention_options,
    query_SOAD_intervention_options,
    query_PD_intervention_options,
    query_GAW_intervention_options,
    query_MDD_intervention_options,
    query_PREDD_intervention_options,
    query_SR_intervention_options,
)


class TestReasoner(unittest.TestCase):
    reasoner = init_reasoner()

    def test_reasoner_case0(self):
        query = query_SEAD_intervention_options(self.reasoner, [SEAD0])
        expected_interventions = set([ISEAD3, ISEAD4])
        self.assertTrue(query == expected_interventions)

        query = query_SEAD_intervention_options(self.reasoner, [SEAD0, SEAD4])
        expected_interventions = set([ISEAD3, ISEAD4])
        self.assertTrue(query == expected_interventions)

    def test_reasoner_case1(self):
        query = query_GAW_intervention_options(self.reasoner, [GAW0])
        expected_interventions = set([IGAW0, IGAW1])
        self.assertTrue(query == expected_interventions)

        query = query_GAW_intervention_options(self.reasoner, [GAW0, GAW8])
        expected_interventions = set([IGAW0, IGAW1, IGAW2])
        self.assertTrue(query == expected_interventions)

    def test_reasoner_case2(self):
        query = query_SOAD_intervention_options(self.reasoner, [SOAD6, SOAD5])
        expected_interventions = set([ISOAD1, ISOAD2])
        self.assertTrue(query == expected_interventions)

        query = query_SOAD_intervention_options(self.reasoner, [SOAD1, SOAD2])
        expected_interventions = set([ISOAD1])
        self.assertTrue(query == expected_interventions)

    def test_reasoner_case3(self):
        query = query_PD_intervention_options(self.reasoner, [PD11])
        expected_interventions = set([IPD0, IPD2, IPD4])
        self.assertTrue(query == expected_interventions)

        query = query_PD_intervention_options(self.reasoner, [PD0, PD5])
        expected_interventions = set([IPD2, IPD3, IPD4])
        self.assertTrue(query == expected_interventions)

    def test_reasoner_case4(self):
        query = query_MDD_intervention_options(self.reasoner, [MDD17])
        expected_interventions = set([IMDD3, IMDD5])
        self.assertTrue(query == expected_interventions)

        query = query_MDD_intervention_options(self.reasoner, [MDD6, MDD7, MDD8])
        expected_interventions = set([IMDD0, IMDD2, IMDD4, IMDD5])
        self.assertTrue(query == expected_interventions)

    def test_reasoner_case5(self):
        query = query_PREDD_intervention_options(self.reasoner, [PREDD9])
        expected_interventions = set([IPREDD0, IPREDD6])
        self.assertTrue(query == expected_interventions)

        query = query_PREDD_intervention_options(
            self.reasoner, [PREDD7, PREDD18, PREDD19]
        )
        expected_interventions = set([IPREDD0, IPREDD1, IPREDD4, IPREDD5, IPREDD6])
        self.assertTrue(query == expected_interventions)

    def test_reasoner_case6(self):
        query = query_SR_intervention_options(self.reasoner, [SR3])
        expected_interventions = set([ISR2])
        self.assertTrue(query == expected_interventions)

        query = query_SR_intervention_options(self.reasoner, [SR0, SR5])
        expected_interventions = set([ISR0, ISR1, ISR2])
        self.assertTrue(query == expected_interventions)
