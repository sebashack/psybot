import unittest
from symptom_graph import SymptomGraph
from symptom_graph import ROOT


class TestSymptomGraph(unittest.TestCase):
    def test_finds_shortest_paths_containing_case0(self):
        dg = SymptomGraph(
            [
                (ROOT, 1, 0.4),
                (ROOT, 2, 0.2),
                (ROOT, 3, 0.3),
                (1, 4, 0.5),
                (4, 7, 0.4),
                (7, 9, 0.2),
                (2, 5, 0.5),
                (5, 7, 0.5),
                (3, 6, 0.4),
                (6, 8, 0.3),
            ]
        )

        dg.initialize()

        found_paths = dg.shortest_paths_containing([7, 8, 9])
        expected_paths = [
            [(0, 2, 0.2), (2, 5, 0.5), (5, 7, 0.5), (7, 9, 0.2)],
            [(0, 3, 0.3), (3, 6, 0.4), (6, 8, 0.3)],
        ]

        self.assertTrue(found_paths == expected_paths)

    def test_finds_shortest_paths_containing_case1(self):
        dg = SymptomGraph(
            [
                (ROOT, 1, 0.4),
                (ROOT, 2, 0.2),
                (ROOT, 3, 0.3),
                (1, 4, 0.5),
                (4, 7, 0.4),
                (7, 9, 0.2),
                (2, 5, 0.5),
                (5, 7, 0.5),
                (3, 6, 0.4),
                (6, 8, 0.3),
                (8, 7, 0.1),
            ]
        )

        dg.initialize()

        found_paths = dg.shortest_paths_containing([7, 8, 9])
        expected_paths = [
            [(0, 3, 0.3), (3, 6, 0.4), (6, 8, 0.3), (8, 7, 0.1), (7, 9, 0.2)]
        ]

        self.assertTrue(found_paths == expected_paths)

    def test_finds_shortest_paths_containing_case2(self):
        dg = SymptomGraph(
            [
                (ROOT, 1, 0.4),
                (ROOT, 2, 0.2),
                (ROOT, 3, 0.3),
                (1, 4, 0.5),
                (4, 7, 0.4),
                (7, 9, 0.4),
                (2, 5, 0.5),
                (5, 7, 0.5),
                (3, 6, 0.4),
                (6, 8, 0.3),
                (6, 9, 0.1),
            ]
        )

        dg.initialize()

        found_paths = dg.shortest_paths_containing([7, 8, 9])
        expected_paths = [
            [(0, 3, 0.3), (3, 6, 0.4), (6, 9, 0.1)],
            [(0, 3, 0.3), (3, 6, 0.4), (6, 8, 0.3)],
            [(0, 2, 0.2), (2, 5, 0.5), (5, 7, 0.5)],
        ]

        self.assertTrue(found_paths == expected_paths)

    def test_finds_shortest_paths_containing_case3(self):
        dg = SymptomGraph(
            [
                (ROOT, 1, 0.4),
                (ROOT, 2, 0.2),
                (ROOT, 3, 0.3),
                (1, 4, 0.5),
                (4, 7, 0.4),
                (7, 9, 0.4),
                (2, 5, 0.5),
                (5, 7, 0.5),
                (3, 6, 0.4),
                (6, 8, 0.3),
                (8, 9, 0.1),
            ]
        )

        dg.initialize()

        found_paths = dg.shortest_paths_containing([7, 8, 9])
        expected_paths = [
            [(0, 3, 0.3), (3, 6, 0.4), (6, 8, 0.3), (8, 9, 0.1)],
            [(0, 2, 0.2), (2, 5, 0.5), (5, 7, 0.5)],
        ]

        self.assertTrue(found_paths == expected_paths)

    def test_finds_shortest_paths_containing_case4(self):
        dg = SymptomGraph(
            [
                (ROOT, 1, 0.4),
                (ROOT, 2, 0.2),
                (ROOT, 3, 0.3),
                (1, 4, 0.5),
                (4, 7, 0.4),
                (7, 9, 0.4),
                (2, 6, 0.5),
                (6, 8, 0.6),
                (8, 9, 0.1),
                (3, 6, 0.5),
                (4, 5, 0.2),
                (5, 8, 0.5),
                (5, 9, 0.1),
            ]
        )

        dg.initialize()

        found_paths = dg.shortest_paths_containing([7, 8, 9])
        expected_paths = [
            [(0, 1, 0.4), (1, 4, 0.5), (4, 5, 0.2), (5, 9, 0.1)],
            [(0, 2, 0.2), (2, 6, 0.5), (6, 8, 0.6)],
            [(0, 1, 0.4), (1, 4, 0.5), (4, 7, 0.4)],
        ]

        self.assertTrue(found_paths == expected_paths)
