import unittest
from intervention_picker import generate_model, compute_strength, FEMALE, MALE


class TestInterventionPicker(unittest.TestCase):
    model = generate_model()

    # rule 1
    def test_intervention_case1(self):
        strength = compute_strength(self.model, FEMALE, 0.1, 0.9, 0.8, 0.1, 0.5)
        self.assertTrue(strength >= 0 and strength <= 0.2)

    # rule 2
    def test_intervention_case2(self):
        strength = compute_strength(self.model, FEMALE, 0.1, 0.9, 0.1, 0.1, 0.5)
        self.assertTrue(strength >= 0.25 and strength <= 0.75)

    # rule 3
    def test_intervention_case3(self):
        strength = compute_strength(self.model, MALE, 0.51, 0.4, 0.1, 0.5, 0.5)
        self.assertTrue(strength >= 0.5 and strength <= 1)

    # rule 4
    def test_intervention_case4(self):
        strength = compute_strength(self.model, FEMALE, 0.1, 0.51, 0.1, 0.85, 0.5)
        self.assertTrue(strength >= 0.5 and strength <= 1)

    # rule 5
    def test_intervention_case5(self):
        strength = compute_strength(self.model, MALE, 0.1, 0.9, 0.1, 0.85, 0.5)
        self.assertTrue(strength >= 0 and strength <= 2)

    # rule 6
    def test_intervention_case6(self):
        strength = compute_strength(self.model, FEMALE, 0.9, 0.1, 0.1, 0.85, 0.5)
        self.assertTrue(strength >= 0 and strength <= 2)

    # rule 7
    def test_intervention_case7(self):
        strength = compute_strength(self.model, FEMALE, 0.1, 0.51, 0.1, 0.85, 0.5)
        self.assertTrue(strength >= 0.5 and strength <= 1)

    # rule 8
    def test_intervention_case8(self):
        strength = compute_strength(self.model, FEMALE, 0.1, 1, 0.1, 0.85, 0.91)
        self.assertTrue(strength >= 0.75 and strength <= 1)

    # rule 9
    def test_intervention_case9(self):
        strength = compute_strength(self.model, MALE, 0.51, 0.1, 0.1, 0.85, 0.5)
        self.assertTrue(strength >= 0.5 and strength <= 1)

    # rule 10
    def test_intervention_case10(self):
        strength = compute_strength(self.model, MALE, 1, 0.1, 0.1, 0.85, 0.91)
        self.assertTrue(strength >= 0.75 and strength <= 1)

    # rule 11
    def test_intervention_case11(self):
        strength = compute_strength(self.model, MALE, 0.51, 0.1, 0.75, 0.85, 0.5)
        self.assertTrue(strength >= 0 and strength <= 0.5)

    # rule 12
    def test_intervention_case12(self):
        strength = compute_strength(self.model, MALE, 0.1, 0.1, 0.7, 0.1, 0.1)
        self.assertTrue(strength >= 0 and strength <= 0.25)

    # rule 13
    def test_intervention_case13(self):
        strength = compute_strength(self.model, MALE, 0.51, 0.1, 0.75, 0.85, 0.51)
        self.assertTrue(strength >= 0.25 and strength <= 0.75)

    # rule 14
    def test_intervention_case14(self):
        strength = compute_strength(self.model, MALE, 1, 0.5, 0.1, 0.85, 0.91)
        self.assertTrue(strength >= 0.75 and strength <= 1)

    # rule 15
    def test_intervention_case15(self):
        strength = compute_strength(self.model, FEMALE, 0.5, 1, 0.1, 0.85, 0.91)
        self.assertTrue(strength >= 0.75 and strength <= 1)

    # rule 16
    def test_intervention_case16(self):
        strength = compute_strength(self.model, FEMALE, 0.1, 0.5, 0.1, 0.85, 0.41)
        self.assertTrue(strength >= 0.25 and strength <= 0.75)

    # rule 17
    def test_intervention_case17(self):
        strength = compute_strength(self.model, MALE, 0.5, 0.1, 0.1, 0.85, 0.41)
        self.assertTrue(strength >= 0.25 and strength <= 0.75)

    # rule 18
    def test_intervention_case18(self):
        strength = compute_strength(self.model, FEMALE, 0.1, 0.1, 0.1, 0.1, 0.1)
        self.assertTrue(strength >= 0 and strength <= 0.45)
