from datetime import datetime
from os import getcwd, path, mkdir
from random import randrange
from tensorflow import keras
from tensorflow.keras import layers, models
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import math
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras.regularizers import l2

from face_utils import (
    read_images,
    read_sample_from_json,
    write_sample_as_json,
    resize_images,
)


def show_images_3x3(images, text_labels, num_labels, n, offset):
    plt.figure(figsize=(10, 10))
    for i in range(n):
        plt.subplot(3, 3, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(images[i + offset])
        plt.xlabel(text_labels[num_labels[i + offset]])
    plt.show()


def show_classified_images_3x3(images, labels_and_pobs):
    plt.figure(figsize=(10, 10))

    for (i, image) in enumerate(images):
        plt.subplot(3, 3, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(image)
        (label, prob) = labels_and_pobs[i]
        p = "{:.2f}".format(prob)
        plt.xlabel(f"{label}: {p}")

    plt.show()


# This helper was used to transform the dictionary data
# structure into another data structure that is suitable for
# training with Keras.
def label_dict_to_matrix(data_set):
    mx = []
    labels = {}
    num_labels = []

    i = 0
    # since python 3.6+ dicts are insertion-ordered,
    # thus order of labels should not be altered.
    for label, images in data_set.items():
        for image in images:
            mx.append(image)
            num_labels.append(i)
        labels[i] = label
        i += 1

    shuffle(mx, num_labels)

    return (np.array(mx), np.array(num_labels), labels)


# Shuffle data set.
def shuffle(mx, num_labels):
    size = len(mx)

    for _ in range(size):
        i = randrange(size)
        j = randrange(size)

        mx[i], mx[j] = mx[j], mx[i]
        num_labels[i], num_labels[j] = num_labels[j], num_labels[i]


# Train model from scratch.
def train_model(
    training_images, training_labels, num_output_layers, batch_size, epochs
):
    size = training_images.shape[0]
    width = training_images.shape[1]
    height = training_images.shape[2]

    model = emotion_detection_model((width, height, 1), num_output_layers)

    model.compile(
        optimizer="adam",
        loss=tf.keras.losses.SparseCategoricalCrossentropy(),
        metrics=["accuracy"],
    )

    training_images_ = training_images.reshape(size, width, height, 1) / 255.0

    gen = make_data_generator(training_images_, training_labels, batch_size)

    model.fit_generator(
        gen["iterator"], steps_per_epoch=gen["steps_per_epoch"], epochs=epochs
    )

    return model


# Train a previously trained model, that is, one that has been loaded.
def retrain_model(
    model, training_images, training_labels, num_output_layers, batch_size, epochs
):
    size = training_images.shape[0]
    width = training_images.shape[1]
    height = training_images.shape[2]

    training_images_ = training_images.reshape(size, width, height, 1) / 255.0

    gen = make_data_generator(training_images_, training_labels, batch_size)

    model.fit_generator(
        gen["iterator"], steps_per_epoch=gen["steps_per_epoch"], epochs=epochs
    )

    return model


# Partition data-set into training and test data. `percentage` is
# the proportion of test samples.
def partition_sample(training_images, training_labels, percentage):
    assert percentage >= 1 and percentage <= 100
    size = len(training_images)
    test_size = round(size * (percentage / 100))
    training_size = size - test_size

    training_set = (training_images[:training_size], training_labels[:training_size])
    test_set = (training_images[training_size:], training_labels[training_size:])

    return {"training": training_set, "test": test_set}


# Evaluate model's efficacy.
def evaluate_model(model, example_images, example_labels, batch_size):
    size = example_images.shape[0]
    width = example_images.shape[1]
    height = example_images.shape[2]

    example_images_ = example_images.reshape(size, width, height, 1) / 255.0
    gen = make_data_generator(example_images_, example_labels, batch_size)

    (loss, accuracy) = model.evaluate(gen["iterator"], verbose=2)

    return (loss, accuracy)


def save_model_for_training(model, filepath):
    model.save(filepath, save_format="tf")


def save_model_weights(model, filepath):
    model.save(filepath, save_format="h5")


def load_nnmodel(filepath):
    return keras.models.load_model(filepath)


def classify_image(model, text_labels, image):
    width = image.shape[0]
    height = image.shape[1]
    input_ = np.array([image]).reshape(1, width, height, 1)

    prediction = model.predict(input_)[0]

    return get_label(text_labels, prediction)


def classify_images(model, text_labels, images):
    images_ = np.array(images)
    size = images_.shape[0]
    width = images_.shape[1]
    height = images_.shape[2]
    input_ = images_.reshape(size, width, height, 1)

    predictions = model.predict(input_)

    return list(map(lambda p: get_label(text_labels, p), predictions))


def get_label(text_labels, prediction):
    i = np.argmax(prediction)
    predicted_label = text_labels[i]
    probability = prediction[i]

    return (predicted_label, probability)


# Create data generator for data augmentation.
def make_data_generator(data_set, labels, batch_size):
    size = data_set.shape[0]
    width = data_set.shape[1]
    height = data_set.shape[2]

    data_set_ = data_set.reshape(size, width, height, 1)

    datagen = ImageDataGenerator(
        horizontal_flip=True, rotation_range=25, brightness_range=(0.6, 1.0)
    )

    it = datagen.flow(data_set_, labels, batch_size=batch_size)
    steps_per_epoch = math.ceil(len(data_set) / batch_size)

    return {"iterator": it, "steps_per_epoch": steps_per_epoch}


def emotion_detection_model(shape, num_output_layers):
    num_features = shape[1]

    model = models.Sequential()
    # 1fst - Convolution layer
    model.add(
        layers.Conv2D(
            num_features,
            kernel_size=(3, 3),
            activation="relu",
            input_shape=shape,
            data_format="channels_last",
            kernel_regularizer=l2(0.01),
        )
    )
    model.add(
        layers.Conv2D(
            num_features, kernel_size=(3, 3), activation="relu", input_shape=shape
        )
    )
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(layers.Dropout(0.25))

    # 2nd - Convolution layer
    model.add(
        layers.Conv2D(
            2 * num_features, kernel_size=(3, 3), activation="relu", padding="same"
        )
    )
    model.add(layers.BatchNormalization())
    model.add(
        layers.Conv2D(
            2 * num_features, kernel_size=(3, 3), activation="relu", padding="same"
        )
    )
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(layers.Dropout(0.25))

    # 3rd - Convolution layer
    model.add(
        layers.Conv2D(
            4 * num_features, kernel_size=(3, 3), activation="relu", padding="same"
        )
    )
    model.add(layers.BatchNormalization())
    model.add(
        layers.Conv2D(
            4 * num_features, kernel_size=(3, 3), activation="relu", padding="same"
        )
    )
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(layers.Dropout(0.25))

    # 4th - Convolution layer
    model.add(
        layers.Conv2D(
            8 * num_features, kernel_size=(3, 3), activation="relu", padding="same"
        )
    )
    model.add(layers.BatchNormalization())
    model.add(
        layers.Conv2D(
            8 * num_features, kernel_size=(3, 3), activation="relu", padding="same"
        )
    )
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(layers.Dropout(0.25))

    model.add(layers.Flatten())

    model.add(layers.Dense(8 * num_features, activation="relu"))
    model.add(layers.Dropout(0.25))
    model.add(layers.Dense(4 * num_features, activation="relu"))
    model.add(layers.Dropout(0.25))
    model.add(layers.Dense(2 * num_features, activation="relu"))
    model.add(layers.Dropout(0.25))

    model.add(layers.Dense(num_output_layers, activation="softmax"))

    return model


# Training pipeline
def label_images_and_store_json(width, height, outpath):
    happy_path = path.join(getcwd(), "faces/happy")
    sad_path = path.join(getcwd(), "faces/sad")
    neutral_path = path.join(getcwd(), "faces/neutral")

    try:
        mkdir(happy_path)
        mkdir(sad_path)
        mkdir(neutral_path)
    except OSError:
        print("'trained-models' DIR already exists")

    happy_images = read_images(happy_path)
    happy_rois = resize_images(happy_images, width, height)

    sad_images = read_images(sad_path)
    sad_rois = resize_images(sad_images, width, height)

    neutral_images = read_images(neutral_path)
    neutral_rois = resize_images(neutral_images, width, height)

    labeled_images = {"happy": happy_rois, "sad": sad_rois, "neutral": neutral_rois}

    json_data_path = path.join(getcwd(), outpath)
    write_sample_as_json(labeled_images, json_data_path)


def train_model_from_json(json_data_path, batch_size, epochs, test_data_percentage=10):
    sample = read_sample_from_json(json_data_path)

    (sample_imgs, numeric_labels, text_labels) = label_dict_to_matrix(sample)
    data_set = partition_sample(
        sample_imgs, numeric_labels, percentage=test_data_percentage
    )
    models_dir = path.join(getcwd(), "trained-models")

    try:
        mkdir(models_dir)
    except OSError:
        print("'trained-models' DIR already exists")

    num_output_layers = len(text_labels)
    timestamp = datetime.now()
    timestamp = timestamp.strftime("%Y-%m-%d-%H:%M:%S")
    model_name = str(timestamp) + "--model"

    trained_model = train_model(
        data_set["training"][0],
        data_set["training"][1],
        num_output_layers,
        batch_size=batch_size,
        epochs=epochs,
    )

    save_model_weights(trained_model, path.join(models_dir, model_name))
    evaluation = evaluate_model(
        trained_model, data_set["test"][0], data_set["test"][1], batch_size=4
    )

    print("######################")
    print("######################")
    print(f"loss: {evaluation[0]}, accuracy: {evaluation[1]}")
    print(f"labels: {text_labels}")
