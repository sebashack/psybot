import random
from symptoms import MDD_GRAPH, SR_GRAPH, SOAD_GRAPH, GAW_GRAPH
from intervention_picker import compute_strength, MALE
from reasoner import (
    query_MDD_intervention_options,
    query_SR_intervention_options,
    query_SOAD_intervention_options,
    query_GAW_intervention_options,
    load_knowledge,
    init_reasoner,
)
from face_utils import webcam_loop, resize_image
from neural_network_utils import classify_image
from tagger import tokenize
from grammar import pass_parser
from constants import *


def text_loop(lock, current_mood, fzmodel, nlp):
    kb = init_reasoner()
    load_knowledge(kb)

    while True:
        user_input = input("Usted: ")
        tokens = tokenize(nlp, user_input.strip())
        detected_symps = pass_parser(tokens, padding=False)
        result = determine_most_relevant_disorder(detected_symps)

        if result is not None:
            disorder, symptoms = result
            lock.acquire()
            try:
                mood = current_mood["mood"]

                if mood is not None:
                    r = random.uniform(0, 1)
                    intervention = determine_intervention(
                        kb, fzmodel, (disorder, symptoms), mood, r
                    )
                    if len(symptoms[1]) > 0:
                        print(
                            "He detectado los siguientes indicios de tristeza y ansiedad:"
                        )
                        for s in symptoms[1]:
                            if s in symptom_code_to_nl:
                                print(f"--> {symptom_code_to_nl[s]}")

                        if intervention[0] in intervention_code_to_nl:
                            print("\n", end="")
                            print(
                                f"Te recomiendo {intervention_code_to_nl[intervention[0]]}."
                            )
                            print("\n", end="")
                    else:
                        print("Cuéntame más sobre tu emociones y estado anímico... :)")
                else:
                    print("Porfavor, mira hacia la camara... :)")

            finally:
                lock.release()


def extract_face_mood(nnmodel, image):
    width = 64
    height = 64
    image_ = resize_image(image, width, height)
    label, probab = classify_image(nnmodel, ["happy", "sad", "neutral"], image_)

    mood_scale = 0.5
    if label == "happy":
        if probab < 0.5:
            return 0.5
        else:
            return probab
    elif label == "sad":
        if probab < 0.5:
            return 0.5
        else:
            return 1 - probab

    return mood_scale


def update_face_loop(lock, current_image):
    webcam_loop(lock, current_image)


def determine_intervention(kb, fzmodel, disorder, current_mood, helpfulness):
    label = disorder[0]
    weight, symptoms = disorder[1]
    interventions = determine_interventions(kb, disorder)
    male_risk = male_disorder_risk[label]
    female_risk = male_disorder_risk[label]

    most_relevant_intv = None
    for intv in interventions:
        strength = compute_strength(
            fzmodel, MALE, male_risk, female_risk, current_mood, weight, helpfulness
        )
        if most_relevant_intv is None:
            most_relevant_intv = (intv, strength)
        elif strength > most_relevant_intv[1]:
            most_relevant_intv = (intv, strength)

    return most_relevant_intv


male_disorder_risk = {
    "mdd": 0.6,
    "sr": 0.6,
    "soad": 0.5,
    "gaw": 0.5,
}

female_disorder_risk = {
    "mdd": 0.7,
    "sr": 0.5,
    "soad": 0.6,
    "gaw": 0.5,
}


def determine_interventions(kb, disorder):
    label = disorder[0]
    _, symptoms = disorder[1]

    if label == "mdd":
        return query_MDD_intervention_options(kb, symptoms)

    if label == "sr":
        return query_SR_intervention_options(kb, symptoms)

    if label == "soad":
        return query_SOAD_intervention_options(kb, symptoms)

    if label == "gaw":
        return query_GAW_intervention_options(kb, symptoms)


def determine_most_relevant_disorder(symptoms):
    disorders = [
        ("mdd", mdd_risk(symptoms)),
        ("sr", sr_risk(symptoms)),
        ("soad", soad_risk(symptoms)),
        ("gaw", gaw_risk(symptoms)),
    ]
    disorder = sorted(disorders, key=lambda d: d[1][0], reverse=True)[0]
    if len(disorder[1][1]) > 0 and disorder[1][0] <= 0:
        return None
    else:
        return disorder


def mdd_risk(symptoms):
    nodes, paths = MDD_GRAPH.shortest_paths_containing(symptoms)
    return (get_weights_avg(paths), nodes)


def sr_risk(symptoms):
    nodes, paths = SR_GRAPH.shortest_paths_containing(symptoms)
    return (get_weights_avg(paths), nodes)


def soad_risk(symptoms):
    nodes, paths = SOAD_GRAPH.shortest_paths_containing(symptoms)
    return (get_weights_avg(paths), nodes)


def gaw_risk(symptoms):
    nodes, paths = GAW_GRAPH.shortest_paths_containing(symptoms)
    return (get_weights_avg(paths), nodes)


def get_weights_avg(paths):
    if len(paths) < 1:
        return 0
    else:
        ws = []
        for p in paths:
            ws += map(lambda s: 1 - s[2], p)
        return sum(ws) / len(ws)


intervention_code_to_nl = {
    IGAW0: "hablar con un terapeuta profesional",
    IGAW1: "meditar con música relajante y ambiental",
    IGAW2: "conversar de tu problema con amigos y familiares",
    IMDD0: "hablar con un terapeuta profesional",
    IMDD1: "mirar el lado positivo de las cosas. Todo estará bien",
    IMDD2: "desarrollar nuevos hábito o hobbies, por ejemplo, practicar un deporte o una actividad artística",
    IMDD3: "mirar una película motivadora como 'en busca de la felicidad' con Will Smith",
    IMDD4: "meditar y hacer ejercicio",
    IMDD5: "conversar de tu problema con amigos y familiares",
    ISOAD0: "hablar con un terapeuta profesional",
    ISOAD1: "conversar de tu problema con amigos y familiares",
    ISOAD2: "mirar una película motivadora como 'en busca de la felicidad' con Will Smith",
    ISOAD3: "el siguiente consejo: se fiel a ti mismo en lo que haces. Así, cosas fascinantes te pasarán",
    ISR0: "hablar con un terapeuta profesional",
    ISR1: "mirar el lado positivo de las cosas. Todo estará bien",
    ISR2: "desarrollar nuevos hábito o hobbies, por ejemplo, practicar un deporte o una actividad artística",
}


symptom_code_to_nl = {
    GAW0: "preocupación y ansiedad excesivas",
    GAW1: "nerviosismo",
    GAW2: "irritabilidad",
    GAW3: "tensión",
    GAW4: "fatigarse fácilmente",
    GAW5: "problemas para dormir",
    GAW6: "insomnia",
    GAW7: "dificultad para concentrarse",
    GAW8: "deterioro social u ocupacional",
    MDD0: "estado de ánimo depresivo",
    MDD2: "sentimiento de tristeza",
    MDD3: "sentimiento de vacio",
    MDD4: "ideación suicida sin plan",
    MDD5: "disminución marcada del placer",
    MDD6: "cambios de peso",
    MDD7: "apetito anormal",
    MDD8: "pérdida de peso",
    MDD9: "fatiga o poca energía",
    MDD10: "desorden de sueño",
    MDD11: "hiperinsomnia",
    MDD12: "aumento de peso",
    MDD13: "insomnia",
    MDD14: "cambios psicomotores",
    MDD15: "sentimiento de retardo",
    MDD16: "sentirse agitado",
    MDD17: "bajo autoestima",
    MDD18: "sentimiento de poca valía",
    MDD19: "sentimiento de culpa",
    MDD20: "pensamientos de muerte",
    MDD21: "intento de suicidio",
    MDD22: "plan de cometer suicidio",
    SR0: "historial de intentos de suicidio",
    SR1: "episodios depresivos",
    SR2: "diagnóstico de una enfermedad severa",
    SR3: "vivir o estar solo",
    SR4: "hombre soltero",
    SR5: "sentimientos profundos de desesperanza",
    SR6: "ansiedad por separación",
    SR7: "historia de abuso infantil",
    SOAD0: "sudoración",
    SOAD1: "evitar situaciones sociales",
    SOAD2: "miedo en situaciones sociales",
    SOAD3: "deterioro en areas ocupacionales",
    SOAD4: "miedo de ofender a alguien",
    SOAD5: "miedo al rechazo",
    SOAD6: "miedo de evaluación negativa",
    SOAD7: "aumento de presión",
    SOAD8: "temblor",
}
