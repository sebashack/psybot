from spacy import displacy
import spacy
import unicodedata


def load_model():
    return spacy.load("es_core_news_md")


def tag_spanish_text(nlp, txt):
    tokens = []
    for token in nlp(txt):
        props = {
            "word": token.text,
            "lemma": token.lemma_,
            "lexeme": token.lex.norm_,
            "pos": token.pos_,
            "isPunct": token.is_punct,
            "dependency": token.dep_,
        }
        tokens.append(props)

    return tokens


def visualize_dependencies(nlp, txt):
    doc = nlp(txt)
    displacy.serve(doc, style="dep")


def strip_accents(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


def tokenize(nlp, txt):
    tokens = nlp(txt)
    txt = ""
    for i, tk in enumerate(tokens):
        is_end = i == len(tokens) - 1
        if is_end:
            txt += strip_accents(tk.lemma_)
        else:
            txt += strip_accents(tk.lemma_) + " "
    return txt
