from lark import Lark, Transformer
from constants import (
    MDD2,
    GAW0,
    SOAD5,
    PREDD1,
    MDD20,
    MDD9,
    MDD3,
    GAW2,
    SR3,
    MDD13,
    SR7,
    GAW5,
    MDD6,
    GAW4,
    MDD17,
    SR0,
)


def quote(txt):
    return '"' + txt + '"'


sadness_adjs = [
    "triste",
    "deprimido",
    "destrozado",
    "desolado",
    "decaido",
    "bajoniado",
    "descorazonado",
    "despechado",
    "sin esperanza",
    "mal",
]
SADNESS_ADJS = "|".join(map(quote, sadness_adjs))

anxiety_adjs = [
    "abrumado",
    "afanado",
    "agobiado",
    "ansioso",
    "asustado",
    "estresado" "impaciente",
    "nervioso",
    "preocupado",
]
ANXIETY_ADJS = "|".join(map(quote, anxiety_adjs))

social_acceptance_adjs = [
    "aceptado",
    "querido",
    "amado",
    "comodo",
    "apreciado",
    "valorado",
    "competente",
    "acompanado",
    "deseado",
]
SOCIAL_ACCEPTANCE_ADJS = "|".join(map(quote, social_acceptance_adjs))

low_energy_nouns = [
    "gana",
    "aliento",
    "deseo",
    "desear",
    "animo",
    "fuerza",
    "energia",
    "motivacion",
]
LOW_ENERGY_NOUNS = "|".join(map(quote, low_energy_nouns))

danger_verbs = [
    "asesinar",
    "desaparecer",
    "herir",
    "lastimar",
    "matar",
    "morir",
    "violentar",
]
DANGER_VERBS = "|".join(map(quote, danger_verbs))

selfesteem_verbs = [
    "odiar",
    "detestar",
    "despreciar",
]
SELFESTEEM_VERBS = "|".join(map(quote, selfesteem_verbs))

emptiness_adjs = [
    "vacio",
    "desesperanzado",
    "desalmado",
    "hueco",
    "sin sentido",
    "sin proposito",
]
EMPTINESS_ADJS = "|".join(map(quote, emptiness_adjs))

alone_words = [
    "solo",
    "soltero",
    "aislado",
]
ALONE_WORDS = "|".join(map(quote, alone_words))


def mk_grammar(padding=False):
    PAD = ""
    if padding:
        PAD = "ANY_CHAR*"

    grammar = f"""
    sentence: {PAD} pron_ser_adj {PAD}
            | {PAD} pron_tener_noun {PAD}
            | {PAD} pron_haber_sentir {PAD}
            | {PAD} pron_sentir_noun {PAD}
            | {PAD} pron_querer_verb {PAD}
            | {PAD} emptiness_feeling {PAD}
            | {PAD} alone_state {PAD}
            | {PAD} sleep_disorder {PAD}
            | {PAD} child_abuse {PAD}
            | {PAD} irritability_tendency {PAD}
            | {PAD} food_disorder {PAD}

    pron_ser_adj:    "yo"? ("ser"|"estar") adj_conjunction                -> pron_ser_adj_kind_0
    pron_tener_noun: "yo"? ("ni"|"no") "tener" noun_conjunction           -> pron_tener_kind_0
    pron_haber_sentir: "yo"? "yo" "haber" "sentir" adj_conjunction        -> pron_haber_sentir_kind_0
                     | "yo"? DENIAL_ADV "yo" "haber" "sentir" SCL_ACC_ADJ -> pron_haber_sentir_kind_1
    pron_sentir_noun:  "yo"? "sentir" LOW_ADV noun_conjunction            -> pron_sentir_kind_0
                    | "yo" "sentir" adj_conjunction                       -> pron_sentir_kind_1
                    | "yo"? DENIAL_ADV "yo" "sentir" SCL_ACC_ADJ          -> pron_sentir_kind_2
                    | "yo"? "sentir"? "que"? "todo" "yo" SELFESTEEM_VERB  -> pron_sentir_kind_3
    pron_querer_verb:  "yo"? ("querer"|"tener gana de") DANGER_VERB "yo"? -> pron_querer_kind_0
    emptiness_feeling: "yo"? "yo"? ("sentir" | "estar") EMPTINESS_ADJ     -> emptiness_feeling_kind_0
    alone_state:      "yo"? "estar" ALONE_WORD                            -> alone_state_kind_0
    sleep_disorder:   "yo"? ("no" | "nunca") SLEEP_WORD                   -> sleep_disorder_kind_0
                    | "yo"? "no" "haber" SLEEP_WORD                       -> sleep_disorder_kind_1
    child_abuse:  "yo"? "ser" ABUSE_WORD INFANCY_WORD                     -> child_abuse_kind_0
                | "yo"? "haber" "ser" ABUSE_WORD INFANCY_WORD             -> child_abuse_kind_1
                | "yo"? ABUSE_WORD INFANCY_WORD                           -> child_abuse_kind_2
    irritability_tendency: "yo"? "yo" IRRITABILITY_WORD HIGH_FREQUENCY_WORD -> irritability_tendency_kind_0
                         | "yo"? "mantener" IRRITABILITY_WORD               -> irritability_tendency_kind_1
    food_disorder: "yo"? ("comer"|"como") (LOW_FREQUENCY_WORD|HIGH_FREQUENCY_WORD) -> food_disorder_0
                 | "yo"? DENIAL_ADV "comer" LOW_FREQUENCY_WORD                     -> food_disorder_1
                 | "yo"? "tener" ("poco"|"mucho") FOOD_WORD                        -> food_disorder_2


    adj_conjunction: (ANXIETY_ADJ|SAD_ADJ) | (ANXIETY_ADJ|SAD_ADJ) CONJUNCTION? adj_conjunction
    noun_conjunction: LOW_ENERGY_NOUN | LOW_ENERGY_NOUN CONJUNCTION? noun_conjunction

    FOOD_WORD: "apetito"|"gana de comer"|"deseo de comer"
    HIGH_FREQUENCY_WORD: "frecuentemente"|"mucho"|"usualmente"|"con frecuencia"
                        |"casi siempre"|"siempre"|"facil"|"facilmente"|"con facilidad"
    LOW_FREQUENCY_WORD: "poco"|"muy poco"|"de vez en cuando"
    IRRITABILITY_WORD: "irritar"|"enojar"|"desesperar"|"molestar"|"enojo"|"enojado"
    INFANCY_WORD: "en mi infancia"|"de nino"
    ABUSE_WORD: "abusar"|"violar"|"insultar"|"maltratar"
    SLEEP_WORD: "dormir"|"descansar"
    EMPTINESS_ADJ: {EMPTINESS_ADJS}
    ALONE_WORD: {ALONE_WORDS}
    SELFESTEEM_VERB: {SELFESTEEM_VERBS}
    DANGER_VERB: {DANGER_VERBS}
    SCL_ACC_ADJ: {SOCIAL_ACCEPTANCE_ADJS}
    DENIAL_ADV: "ni"|"no"|"nunca"|"jamas"
    CONJUNCTION: "y" | "pero" | "ni"
    LOW_ADV: "falta de" | "poco"
    SAD_ADJ: {SADNESS_ADJS}
    ANXIETY_ADJ: {ANXIETY_ADJS}
    LOW_ENERGY_NOUN: {LOW_ENERGY_NOUNS}
    ANY_CHAR: DIGIT|LETTER|":"|"+"|"-"|"*"|"_"|"."|";"|"("|")"|"$"|"#"|"/"|"#"|"!"|"="

    %ignore "." | "," | ";"
    %import common.DIGIT
    %import common.LETTER
    %import common.NEWLINE
    %import common.WS
    %ignore WS
    """

    return grammar


class TreeToRRule(Transformer):
    def __init__(self, original_txt):
        self.original_txt = original_txt
        self.found_symptoms = set([])

    def sentence(self, items):
        symptoms = self.found_symptoms
        txt = self.original_txt.strip()
        return (symptoms, txt)

    def pron_ser_adj_kind_0(self, items):
        adjs = count_sad_anxiety_adjs(filter_nones_and_tokens(items)[0])
        if adjs["sadness"] > 0:
            self.found_symptoms.add(MDD2)
        if adjs["anxiety"] > 0:
            self.found_symptoms.add(GAW0)
        return None

    def pron_tener_kind_0(self, items):
        self.found_symptoms.add(MDD9)
        self.found_symptoms.add(GAW4)
        return None

    def pron_sentir_kind_0(self, items):
        self.found_symptoms.add(MDD9)
        self.found_symptoms.add(GAW4)
        return None

    def pron_sentir_kind_1(self, items):
        adjs = count_sad_anxiety_adjs(filter_nones_and_tokens(items)[0])
        if adjs["sadness"] > 0:
            self.found_symptoms.add(MDD2)
        if adjs["anxiety"] > 0:
            self.found_symptoms.add(GAW0)
        return None

    def pron_sentir_kind_2(self, items):
        self.found_symptoms.add(SOAD5)
        return None

    def pron_sentir_kind_3(self, items):
        self.found_symptoms.add(PREDD1)
        self.found_symptoms.add(MDD17)
        return None

    def pron_querer_kind_0(self, items):
        self.found_symptoms.add(MDD20)
        self.found_symptoms.add(SR0)
        return None

    def pron_haber_sentir_kind_0(self, items):
        adjs = count_sad_anxiety_adjs(filter_nones_and_tokens(items[0]))
        if adjs["sadness"] > 0:
            self.found_symptoms.add(MDD2)
        if adjs["anxiety"] > 0:
            self.found_symptoms.add(GAW0)
        return None

    def emptiness_feeling_kind_0(self, items):
        self.found_symptoms.add(MDD3)
        return None

    def irritability_tendency_kind_0(self, items):
        self.found_symptoms.add(GAW2)
        return None

    def irritability_tendency_kind_1(self, items):
        self.found_symptoms.add(GAW2)
        return None

    def pron_haber_sentir_kind_1(self, items):
        self.found_symptoms.add(SOAD5)
        return None

    def alone_state_kind_0(self, items):
        self.found_symptoms.add(SR3)
        return None

    def sleep_disorder_kind_0(self, items):
        self.found_symptoms.add(MDD13)
        self.found_symptoms.add(GAW5)
        return None

    def sleep_disorder_kind_1(self, items):
        self.found_symptoms.add(MDD13)
        self.found_symptoms.add(GAW5)
        return None

    def child_abuse_kind_0(self, items):
        self.found_symptoms.add(SR7)
        return None

    def child_abuse_kind_1(self, items):
        self.found_symptoms.add(SR7)
        return None

    def child_abuse_kind_2(self, items):
        self.found_symptoms.add(SR7)
        return None

    def food_disorder_0(self, items):
        self.found_symptoms.add(MDD6)
        return None

    def food_disorder_1(self, items):
        self.found_symptoms.add(MDD6)
        return None

    def food_disorder_2(self, items):
        self.found_symptoms.add(MDD6)
        return None

    def noun_conjunction(self, items):
        ls = []
        for itm in items:
            if itm == int(1):
                ls.append(itm)
            if type(itm) is list:
                ls += itm
        return ls

    def adj_conjunction(self, items):
        ls = []
        for itm in items:
            if type(itm) is str:
                ls.append(itm)
            elif type(itm) is list:
                ls += itm
        return ls

    def SAD_ADJ(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return "sadness"

    def ANXIETY_ADJ(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return "anxiety"

    def LOW_ENERGY_NOUN(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def DANGER_VERB(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def SCL_ACC_ADJ(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def SELFESTEEM_VERB(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def EMPTINESS_ADJ(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def ALONE_WORD(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def SLEEP_WORD(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def INFANCY_WORD(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def ABUSE_WORD(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def HIGH_FREQUENCY_WORD(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def LOW_FREQUENCY_WORD(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def IRRITABILITY_WORD(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def FOOD_WORD(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return int(1)

    def CONJUNCTION(self, items):
        self.original_txt = self.original_txt.replace(items, "", 1)
        return None


def filter_nones_and_tokens(ls):
    def f(v):
        class_name = type(v).__name__
        return v is not None and class_name != "Token"

    return list(filter(f, ls))


def count_sad_anxiety_adjs(items):
    sadness_count = 0
    anxiety_count = 0
    adjs = filter(lambda elt: type(elt) == str, items)
    for v in adjs:
        if v == "sadness":
            sadness_count += 1
        elif v == "anxiety":
            anxiety_count += 1

    return {"sadness": sadness_count, "anxiety": anxiety_count}


parser_no_padding = Lark(
    mk_grammar(padding=False),
    parser="earley",
    ambiguity="resolve",
    lexer="dynamic_complete",
    start="sentence",
)

parser_padding = Lark(
    mk_grammar(padding=True),
    parser="earley",
    ambiguity="resolve",
    lexer="dynamic_complete",
    start="sentence",
)


def parse_ast(txt, padding=False):
    try:
        if padding:
            return parser_padding.parse(txt)
        else:
            return parser_no_padding.parse(txt)
    except Exception:
        return None


def parse_rule(txt, padding=False):
    ast = parse_ast(txt, padding)
    if ast is not None:
        try:
            return TreeToRRule(txt).transform(ast)
        except Exception:
            return None


def remove_first_word(txt):
    words = txt.split(" ", 1)
    return words[1]


def pass_parser(txt, padding=False):
    current_txt = txt.strip()
    results = set([])

    while len(current_txt) > 1:
        result = parse_rule(current_txt, padding)
        if result is None:
            # remove first word
            try:
                current_txt = remove_first_word(current_txt)
            except Exception:
                break
        else:
            desc, left_txt = result
            results = results.union(desc)
            current_txt = left_txt

    return list(results)
