import networkx as nx


ROOT = 0


class SymptomGraph:
    edge_to = {}
    dist_to = {}
    dg = None
    tps = None

    def __init__(self, edges):
        for e in edges:
            assert e[1] != ROOT
            assert e[2] > 0 and e[2] <= 1

        self.dg = nx.DiGraph()
        self.dg.add_weighted_edges_from(edges)

        for node in self.dg.nodes:
            self.dist_to[node] = float("inf")
            self.edge_to[node] = None

        self.dist_to[ROOT] = 0

    def initialize(self):
        self.tps = list(nx.topological_sort(self.dg))
        assert self.tps[0] == ROOT

        for node in self.tps:
            for edge in self.dg.edges(node, data="weight"):
                self.relax(edge)

    def print_shortest_paths(self):
        for i, e in sorted(self.edge_to.items()):
            if e is not None:
                weight = "{:.2f}".format(self.dist_to[i])
                print(f"{i}: ({e[0]})-{e[2]}->({e[1]})) | {weight}")
            else:
                print(f"{i}: --")

    def shortest_paths_containing(self, nodes_):
        nodes = list(filter(lambda n: self.dg.has_node(n), nodes_))

        if len(nodes) == 0:
            return (nodes, [])

        sort_nodes_by_tps(self.tps, nodes)
        nodes.reverse()

        paths = []
        visited_nodes = set({})

        for v in nodes:
            if v is not ROOT:
                path, nodes = self.shortest_path_to(v)

                if not nodes.issubset(visited_nodes):
                    paths.append(path)
                    visited_nodes = visited_nodes.union(nodes)

        return (nodes, paths)

    def shortest_path_to(self, v):
        if v == ROOT:
            return []

        cur_edge = self.edge_to[v]
        path = []
        nodes = set([])

        while cur_edge is not None:
            nodes.add(cur_edge[0])
            nodes.add(cur_edge[1])

            path.insert(0, cur_edge)
            cur_edge = self.edge_to[cur_edge[0]]

        return (path, nodes)

    def relax(self, edge):
        v = edge[0]
        w = edge[1]
        weight = edge[2]

        if self.dist_to[w] > self.dist_to[v] + weight:
            self.dist_to[w] = self.dist_to[v] + weight
            self.edge_to[w] = edge


def sort_nodes_by_tps(tps, nodes):
    indices = {}

    for i, k in zip(range(len(tps)), tps):
        indices[k] = i

    nodes.sort(key=lambda v: indices[v])
