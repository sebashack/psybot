from constants import *
from symptom_graph import SymptomGraph, ROOT

MDD_GRAPH = SymptomGraph(
    [
        (ROOT, MDD0, 0.1),
        (MDD0, MDD1, 0.1),
        (MDD0, MDD2, 0.3),
        (MDD0, MDD3, 0.2),
        (MDD1, MDD4, 0.3),
        (ROOT, MDD5, 0.3),
        (MDD5, MDD3, 0.2),
        (ROOT, MDD6, 0.2),
        (MDD6, MDD7, 0.3),
        (MDD5, MDD7, 0.3),
        (MDD6, MDD8, 0.2),
        (ROOT, MDD9, 0.3),
        (MDD9, MDD11, 0.3),
        (MDD11, MDD12, 0.2),
        (ROOT, MDD10, 0.2),
        (MDD10, MDD11, 0.3),
        (MDD6, MDD12, 0.4),
        (MDD10, MDD13, 0.1),
        (MDD13, MDD8, 0.1),
        (ROOT, MDD14, 0.4),
        (MDD14, MDD15, 0.3),
        (MDD14, MDD16, 0.2),
        (ROOT, MDD17, 0.2),
        (MDD17, MDD18, 0.2),
        (MDD17, MDD19, 0.2),
        (ROOT, MDD20, 0.2),
        (MDD20, MDD21, 0.1),
        (MDD20, MDD22, 0.1),
        (MDD20, MDD4, 0.2),
        (MDD1, MDD4, 0.1),
    ]
)
MDD_GRAPH.initialize()


PREDD_GRAPH = SymptomGraph(
    [
        (ROOT, PREDD0, 0.1),
        (PREDD0, PREDD1, 0.3),
        (PREDD1, PREDD2, 0.2),
        (ROOT, PREDD3, 0.1),
        (PREDD3, PREDD2, 0.4),
        (PREDD0, PREDD4, 0.2),
        (PREDD3, PREDD4, 0.3),
        (PREDD0, PREDD5, 0.4),
        (ROOT, PREDD6, 0.4),
        (PREDD6, PREDD7, 0.4),
        (PREDD7, PREDD5, 0.4),
        (ROOT, PREDD8, 0.2),
        (PREDD8, PREDD9, 0.4),
        (PREDD9, PREDD10, 0.3),
        (ROOT, PREDD11, 0.5),
        (PREDD11, PREDD7, 0.4),
        (ROOT, PREDD12, 0.3),
        (ROOT, PREDD13, 0.3),
        (PREDD12, PREDD13, 0.2),
        (ROOT, PREDD14, 0.3),
        (PREDD13, PREDD14, 0.3),
        (ROOT, PREDD15, 0.3),
        (PREDD15, PREDD16, 0.2),
        (PREDD15, PREDD17, 0.2),
        (PREDD16, PREDD17, 0.3),
        (ROOT, PREDD18, 0.4),
        (ROOT, PREDD19, 0.4),
        (ROOT, PREDD20, 0.4),
        (ROOT, PREDD21, 0.5),
        (ROOT, PREDD22, 0.5),
        (ROOT, PREDD23, 0.5),
    ]
)
PREDD_GRAPH.initialize()


SR_GRAPH = SymptomGraph(
    [
        (ROOT, SR0, 0.3),
        (ROOT, SR1, 0.3),
        (SR0, SR1, 0.1),
        (ROOT, SR3, 0.4),
        (SR3, SR5, 0.2),
        (ROOT, SR4, 0.5),
        (SR4, SR5, 0.3),
        (ROOT, SR2, 0.3),
        (ROOT, SR6, 0.2),
        (ROOT, SR7, 0.2),
    ]
)
SR_GRAPH.initialize()


SEAD_GRAPH = SymptomGraph(
    [
        (ROOT, SEAD0, 0.1),
        (SEAD0, SEAD1, 0.2),
        (SEAD0, SEAD2, 0.2),
        (ROOT, SEAD3, 0.2),
        (ROOT, SEAD4, 0.3),
        (SEAD4, SEAD3, 0.2),
        (ROOT, SEAD5, 0.5),
        (ROOT, SEAD6, 0.3),
        (ROOT, SEAD7, 0.4),
        (ROOT, SEAD8, 0.4),
    ]
)
SEAD_GRAPH.initialize()


SOAD_GRAPH = SymptomGraph(
    [
        (ROOT, SOAD0, 0.3),
        (ROOT, SOAD1, 0.2),
        (ROOT, SOAD2, 0.2),
        (SOAD2, SOAD3, 0.4),
        (SOAD2, SOAD4, 0.4),
        (SOAD2, SOAD5, 0.3),
        (ROOT, SOAD6, 0.3),
        (SOAD6, SOAD5, 0.3),
        (ROOT, SOAD7, 0.3),
        (ROOT, SOAD8, 0.3),
    ]
)
SOAD_GRAPH.initialize()


PD_GRAPH = SymptomGraph(
    [
        (ROOT, PD0, 0.2),
        (ROOT, PD1, 0.7),
        (PD0, PD1, 0.2),
        (ROOT, PD2, 0.4),
        (PD1, PD2, 0.2),
        (ROOT, PD3, 0.7),
        (PD2, PD3, 0.2),
        (ROOT, PD4, 0.2),
        (PD4, PD3, 0.3),
        (ROOT, PD5, 0.5),
        (PD4, PD5, 0.2),
        (PD3, PD6, 0.3),
        (PD5, PD6, 0.1),
        (PD6, PD7, 0.1),
        (ROOT, PD8, 0.5),
        (PD5, PD8, 0.3),
        (ROOT, PD9, 0.3),
        (PD8, PD9, 0.2),
        (ROOT, PD10, 0.1),
        (ROOT, PD11, 0.3),
        (PD10, PD11, 0.1),
        (ROOT, PD12, 0.3),
        (PD12, PD13, 0.1),
        (ROOT, PD14, 0.1),
        (PD13, PD14, 0.1),
        (PD14, PD15, 0.1),
        (PD14, PD0, 0.2),
        (PD0, PD15, 0.1),
    ]
)
PD_GRAPH.initialize()


GAW_GRAPH = SymptomGraph(
    [
        (ROOT, GAW0, 0.2),
        (ROOT, GAW1, 0.1),
        (GAW1, GAW0, 0.1),
        (ROOT, GAW2, 0.5),
        (GAW1, GAW2, 0.3),
        (ROOT, GAW3, 0.3),
        (GAW2, GAW3, 0.2),
        (ROOT, GAW4, 0.6),
        (GAW3, GAW4, 0.2),
        (ROOT, GAW5, 0.3),
        (GAW5, GAW6, 0.2),
        (GAW6, GAW4, 0.1),
        (ROOT, GAW7, 0.3),
        (GAW6, GAW7, 0.1),
        (ROOT, GAW8, 0.2),
        (GAW7, GAW8, 0.1),
        (GAW0, GAW8, 0.1),
    ]
)
GAW_GRAPH.initialize()
