from collections import defaultdict
from os import walk, path
import base64
import cv2
import json
import numpy as np


def webcam_loop(lock, current_image, mirror=False):
    video = cv2.VideoCapture(0)
    while True:
        check, frame = video.read()
        if mirror:
            frame = cv2.flip(frame, 1)

        grey_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # face_images has the detected faces.
        data_faces, face_images = detect_faces(grey_image)

        modified_frame = add_rectangle(frame, data_faces)

        cv2.imshow("mywebcam", modified_frame)

        lock.acquire()
        try:
            if len(face_images) > 0:
                current_image["face"] = face_images[0]
        finally:
            lock.release()

        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()


def detect_faces(reduced_image):
    face_cascade = cv2.data.haarcascades + "haarcascade_frontalface_default.xml"
    face_classifier = cv2.CascadeClassifier(face_cascade)

    rectangles = face_classifier.detectMultiScale(
        reduced_image, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30)
    )

    face_images = []

    for (x, y, w, h) in rectangles:
        ROI = reduced_image[y : y + h, x : x + w]
        face_images.append(ROI)

    return (rectangles, face_images)


def add_rectangle(frame, faces):
    color = (0, 255, 255)
    thickness = 15

    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), color, thickness)

    return frame


def resize_images(images, w, h):
    images_ = list(filter(lambda img: img is not None, images))
    return list(map(lambda img: resize_image(img, w, h), images_))


def resize_image(image, width, height):
    output = cv2.resize(image, (width, height), interpolation=cv2.INTER_NEAREST)

    return output


def show_detected_faces(image, faces):
    print("Found {0} faces".format(len(faces)))
    color = (0, 0, 0)  # Black
    grosor = 2
    for (x, y, w, h) in faces:
        cv2.rectangle(image, (x, y), (x + w, y + h), color, grosor)

    cv2.imshow("found faces", image)
    cv2.waitKey(0)


def read_images(dirpath):
    images = []

    for (_, _, filenames) in walk(dirpath):
        for name in filenames:
            img_path = read_image(path.join(dirpath, name))
            images.append(img_path)

    return images


def read_image(path):
    return cv2.imread(path)


def read_sample_from_json(filepath):
    labeled_images = defaultdict(list)
    with open(filepath, "r") as f:
        data = json.load(f)

        for (label, encoded_imgs) in data.items():
            images = list(map(base64_str_to_np_image, encoded_imgs))
            labeled_images[label] = list(map(to_gray_image, images))

        f.close()

    return labeled_images


def to_gray_image(image):
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return gray_image


def write_sample_as_json(labeled_images, filepath):
    obj = sample_to_json(labeled_images)
    with open(filepath, "w") as f:
        json.dump(obj, f)


def sample_to_json(labeled_images):
    obj = defaultdict(list)
    for (label, images) in labeled_images.items():
        obj[label] = list(map(np_image_to_base64, images))

    return obj


def np_image_to_base64(image):
    retval, buff = cv2.imencode(".jpg", image)
    img_as_text = base64.b64encode(buff)
    return img_as_text.decode("utf-8")


def base64_str_to_np_image(s):
    buff = base64.b64decode(s)
    image_as_np = np.frombuffer(buff, dtype=np.uint8)
    return cv2.imdecode(image_as_np, flags=1)


def show_images(images, delay):
    for image in images:
        cv2.imshow("img", image)
        cv2.waitKey(delay)


def show_images_dict(img_dict, delay):
    for images in img_dict.values():
        for image in images:
            cv2.imshow("img", image)
            cv2.waitKey(delay)
