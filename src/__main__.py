import os
import sys
from os import getcwd, path

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

from tagger import load_model, tokenize
from grammar import pass_parser
from pipeline import (
    determine_most_relevant_disorder,
    determine_intervention,
    update_face_loop,
    extract_face_mood,
    text_loop,
)
from intervention_picker import generate_model
from threading import Thread, Lock
from neural_network_utils import load_nnmodel
import time


def main(argv):
    nlp = load_model()
    fzmodel = generate_model()
    nnmodel = load_nnmodel(
        path.join(getcwd(), "trained-models/2022-04-17-20:23:50--model")
    )

    # Camera
    face_lock = Lock()
    current_image = {"face": None}
    camthread = Thread(
        target=lambda: update_face_loop(face_lock, current_image), args=[]
    )
    camthread.start()

    # Mood detection
    disorder_lock = Lock()
    current_mood = {"mood": None}

    def mood_loop():
        while True:
            disorder_lock.acquire()
            face_lock.acquire()
            try:
                if current_image["face"] is not None:
                    current_mood["mood"] = extract_face_mood(
                        nnmodel, current_image["face"]
                    )
            finally:
                disorder_lock.release()
                face_lock.release()
            time.sleep(1)

    moodthread = Thread(target=mood_loop, args=[])
    moodthread.start()

    # Main thread: Text
    text_loop(disorder_lock, current_mood, fzmodel, nlp)

    camthread.join()
    moodthread.join()


if __name__ == "__main__":
    main(sys.argv[1:])
