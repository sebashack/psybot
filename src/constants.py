#############################
# Major Depressive Disorder #
#############################

# Symptoms
MDD0 = 1000
MDD1 = 1001
MDD2 = 1002
MDD3 = 1003
MDD4 = 1004
MDD5 = 1005
MDD6 = 1006
MDD7 = 1007
MDD8 = 1008
MDD9 = 1009
MDD10 = 1010
MDD11 = 1011
MDD12 = 1012
MDD13 = 1013
MDD14 = 1014
MDD15 = 1015
MDD16 = 1016
MDD17 = 1017
MDD18 = 1018
MDD19 = 1019
MDD20 = 1020
MDD21 = 1021
MDD22 = 1022

# Interventions
IMDD0 = 501
IMDD1 = 502
IMDD2 = 503
IMDD3 = 501
IMDD4 = 504
IMDD5 = 500

###################################
# Premenstrual Dysphoric Disorder #
###################################

# Symptoms
PREDD0 = 1100
PREDD1 = 1101
PREDD2 = 1102
PREDD3 = 1103
PREDD4 = 1104
PREDD5 = 1105
PREDD6 = 1106
PREDD7 = 1107
PREDD8 = 1108
PREDD9 = 1109
PREDD10 = 1110
PREDD11 = 1111
PREDD12 = 1112
PREDD13 = 1113
PREDD14 = 1114
PREDD15 = 1115
PREDD16 = 1116
PREDD17 = 1117
PREDD18 = 1118
PREDD19 = 1119
PREDD20 = 1120
PREDD21 = 1121
PREDD22 = 1122
PREDD23 = 1123

# Interventions
IPREDD0 = 501
IPREDD1 = 505
IPREDD2 = 503
IPREDD3 = 506
IPREDD4 = 507
IPREDD5 = 504
IPREDD6 = 500
IPREDD7 = 502

################
# Suicide risk #
################

# Symptoms
SR0 = 1200
SR1 = 1201
SR2 = 1202
SR3 = 1203
SR4 = 1204
SR5 = 1205
SR6 = 1206
SR7 = 1207

# Interventions
ISR0 = 501
ISR1 = 502
ISR2 = 503

###############################
# Separation anxiety disorder #
###############################

# Symptoms
SEAD0 = 2000
SEAD1 = 2001
SEAD2 = 2002
SEAD3 = 2003
SEAD4 = 2004
SEAD5 = 2005
SEAD6 = 2006
SEAD7 = 2007
SEAD8 = 2008

# Interventions
ISEAD0 = 501
ISEAD1 = 505
ISEAD2 = 506
ISEAD3 = 507
ISEAD4 = 500

###########################
# Social anxiety disorder #
###########################

# Symptoms
SOAD0 = 2100
SOAD1 = 2101
SOAD2 = 2102
SOAD3 = 2103
SOAD4 = 2104
SOAD5 = 2105
SOAD6 = 2106
SOAD7 = 2107
SOAD8 = 2108
SOAD9 = 2109

# Interventions
ISOAD0 = 501
ISOAD1 = 500
ISOAD2 = 505
ISOAD3 = 506

##################
# Panic Disorder #
##################

# Symptoms
PD0 = 2200
PD1 = 2201
PD2 = 2202
PD3 = 2203
PD4 = 2204
PD5 = 2205
PD6 = 2206
PD7 = 2207
PD8 = 2208
PD9 = 2209
PD10 = 2210
PD11 = 2211
PD12 = 2212
PD13 = 2213
PD14 = 2214
PD15 = 2215

# Interventions
IPD0 = 501
IPD1 = 506
IPD2 = 505
IPD3 = 504
IPD4 = 507

################################
# Generalized Anxiety Disorder #
################################

# Symptoms
GAW0 = 2300
GAW1 = 2301
GAW2 = 2302
GAW3 = 2303
GAW4 = 2304
GAW5 = 2305
GAW6 = 2306
GAW7 = 2307
GAW8 = 2308

# Interventions
IGAW0 = 501
IGAW1 = 505
IGAW2 = 500
