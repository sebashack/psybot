from pyswip import Prolog

from constants import *


def load_knowledge(prolog):
    # Separation anxiety disorder
    prolog.assertz(f"has_SEAD_intervention({SEAD0},{ISEAD3})")
    prolog.assertz(f"has_SEAD_intervention({SEAD0},{ISEAD4})")
    prolog.assertz(f"has_SEAD_intervention({SEAD1},{ISEAD2})")
    prolog.assertz(f"has_SEAD_intervention({SEAD1},{ISEAD3})")
    prolog.assertz(f"has_SEAD_intervention({SEAD1},{ISEAD4})")
    prolog.assertz(f"has_SEAD_intervention({SEAD2},{ISEAD2})")
    prolog.assertz(f"has_SEAD_intervention({SEAD2},{ISEAD3})")
    prolog.assertz(f"has_SEAD_intervention({SEAD2},{ISEAD4})")
    prolog.assertz(f"has_SEAD_intervention({SEAD3},{ISEAD3})")
    prolog.assertz(f"has_SEAD_intervention({SEAD3},{ISEAD4})")
    prolog.assertz(f"has_SEAD_intervention({SEAD4},{ISEAD3})")
    prolog.assertz(f"has_SEAD_intervention({SEAD4},{ISEAD4})")
    prolog.assertz(f"has_SEAD_intervention({SEAD5},{ISEAD0})")
    prolog.assertz(f"has_SEAD_intervention({SEAD5},{ISEAD4})")
    prolog.assertz(f"has_SEAD_intervention({SEAD6},{ISEAD0})")
    prolog.assertz(f"has_SEAD_intervention({SEAD6},{ISEAD1})")
    prolog.assertz(f"has_SEAD_intervention({SEAD6},{ISEAD3})")
    prolog.assertz(f"has_SEAD_intervention({SEAD7},{ISEAD0})")
    prolog.assertz(f"has_SEAD_intervention({SEAD8},{ISEAD0})")

    prolog.assertz(
        r"all_SEAD_intervention_options(I,[S|TAIL]) :- has_SEAD_intervention(S, I) ; all_SEAD_intervention_options(I,TAIL)"
    )

    # Social anxiety disorder
    prolog.assertz(f"has_SOAD_intervention({SOAD0},{ISOAD0})")
    prolog.assertz(f"has_SOAD_intervention({SOAD1},{ISOAD1})")
    prolog.assertz(f"has_SOAD_intervention({SOAD2},{ISOAD1})")
    prolog.assertz(f"has_SOAD_intervention({SOAD3},{ISOAD0})")
    prolog.assertz(f"has_SOAD_intervention({SOAD3},{ISOAD1})")
    prolog.assertz(f"has_SOAD_intervention({SOAD4},{ISOAD1})")
    prolog.assertz(f"has_SOAD_intervention({SOAD4},{ISOAD3})")
    prolog.assertz(f"has_SOAD_intervention({SOAD5},{ISOAD1})")
    prolog.assertz(f"has_SOAD_intervention({SOAD5},{ISOAD2})")
    prolog.assertz(f"has_SOAD_intervention({SOAD6},{ISOAD1})")
    prolog.assertz(f"has_SOAD_intervention({SOAD6},{ISOAD2})")
    prolog.assertz(f"has_SOAD_intervention({SOAD7},{ISOAD0})")
    prolog.assertz(f"has_SOAD_intervention({SOAD8},{ISOAD0})")

    prolog.assertz(
        r"all_SOAD_intervention_options(I,[S|TAIL]) :- has_SOAD_intervention(S, I) ; all_SOAD_intervention_options(I,TAIL)"
    )

    # Panic Disorder
    prolog.assertz(f"has_PD_intervention({PD0},{IPD2})")
    prolog.assertz(f"has_PD_intervention({PD0},{IPD3})")
    prolog.assertz(f"has_PD_intervention({PD1},{IPD0})")
    prolog.assertz(f"has_PD_intervention({PD2},{IPD0})")
    prolog.assertz(f"has_PD_intervention({PD2},{IPD4})")
    prolog.assertz(f"has_PD_intervention({PD3},{IPD0})")
    prolog.assertz(f"has_PD_intervention({PD3},{IPD3})")
    prolog.assertz(f"has_PD_intervention({PD3},{IPD4})")
    prolog.assertz(f"has_PD_intervention({PD4},{IPD2})")
    prolog.assertz(f"has_PD_intervention({PD5},{IPD3})")
    prolog.assertz(f"has_PD_intervention({PD5},{IPD4})")
    prolog.assertz(f"has_PD_intervention({PD6},{IPD0})")
    prolog.assertz(f"has_PD_intervention({PD7},{IPD2})")
    prolog.assertz(f"has_PD_intervention({PD7},{IPD3})")
    prolog.assertz(f"has_PD_intervention({PD8},{IPD0})")
    prolog.assertz(f"has_PD_intervention({PD8},{IPD4})")
    prolog.assertz(f"has_PD_intervention({PD9},{IPD0})")
    prolog.assertz(f"has_PD_intervention({PD10},{IPD1})")
    prolog.assertz(f"has_PD_intervention({PD10},{IPD4})")
    prolog.assertz(f"has_PD_intervention({PD11},{IPD0})")
    prolog.assertz(f"has_PD_intervention({PD11},{IPD2})")
    prolog.assertz(f"has_PD_intervention({PD11},{IPD4})")
    prolog.assertz(f"has_PD_intervention({PD12},{IPD1})")
    prolog.assertz(f"has_PD_intervention({PD12},{IPD4})")
    prolog.assertz(f"has_PD_intervention({PD13},{IPD0})")
    prolog.assertz(f"has_PD_intervention({PD13},{IPD4})")
    prolog.assertz(f"has_PD_intervention({PD14},{IPD1})")
    prolog.assertz(f"has_PD_intervention({PD15},{IPD0})")

    prolog.assertz(
        r"all_PD_intervention_options(I,[S|TAIL]) :- has_PD_intervention(S, I) ; all_PD_intervention_options(I,TAIL)"
    )

    # General Anxiety Disorder
    prolog.assertz(f"has_GAW_intervention({GAW0},{IGAW0})")
    prolog.assertz(f"has_GAW_intervention({GAW0},{IGAW1})")
    prolog.assertz(f"has_GAW_intervention({GAW1},{IGAW1})")
    prolog.assertz(f"has_GAW_intervention({GAW1},{IGAW2})")
    prolog.assertz(f"has_GAW_intervention({GAW2},{IGAW1})")
    prolog.assertz(f"has_GAW_intervention({GAW2},{IGAW2})")
    prolog.assertz(f"has_GAW_intervention({GAW3},{IGAW0})")
    prolog.assertz(f"has_GAW_intervention({GAW4},{IGAW0})")
    prolog.assertz(f"has_GAW_intervention({GAW5},{IGAW2})")
    prolog.assertz(f"has_GAW_intervention({GAW6},{IGAW0})")
    prolog.assertz(f"has_GAW_intervention({GAW6},{IGAW2})")
    prolog.assertz(f"has_GAW_intervention({GAW7},{IGAW1})")
    prolog.assertz(f"has_GAW_intervention({GAW8},{IGAW2})")

    prolog.assertz(
        r"all_GAW_intervention_options(I,[S|TAIL]) :- has_GAW_intervention(S, I) ; all_GAW_intervention_options(I,TAIL)"
    )

    # Major Depressive Disorder
    prolog.assertz(f"has_MDD_intervention({MDD0},{IMDD1})")
    prolog.assertz(f"has_MDD_intervention({MDD0},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD0},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD1},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD1},{IMDD1})")
    prolog.assertz(f"has_MDD_intervention({MDD1},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD2},{IMDD1})")
    prolog.assertz(f"has_MDD_intervention({MDD2},{IMDD2})")
    prolog.assertz(f"has_MDD_intervention({MDD3},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD3},{IMDD2})")
    prolog.assertz(f"has_MDD_intervention({MDD4},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD4},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD5},{IMDD2})")
    prolog.assertz(f"has_MDD_intervention({MDD6},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD6},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD7},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD7},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD8},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD8},{IMDD2})")
    prolog.assertz(f"has_MDD_intervention({MDD9},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD9},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD10},{IMDD2})")
    prolog.assertz(f"has_MDD_intervention({MDD10},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD11},{IMDD2})")
    prolog.assertz(f"has_MDD_intervention({MDD11},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD12},{IMDD2})")
    prolog.assertz(f"has_MDD_intervention({MDD13},{IMDD2})")
    prolog.assertz(f"has_MDD_intervention({MDD13},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD13},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD14},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD15},{IMDD3})")
    prolog.assertz(f"has_MDD_intervention({MDD15},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD16},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD16},{IMDD4})")
    prolog.assertz(f"has_MDD_intervention({MDD17},{IMDD3})")
    prolog.assertz(f"has_MDD_intervention({MDD17},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD18},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD18},{IMDD1})")
    prolog.assertz(f"has_MDD_intervention({MDD18},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD19},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD19},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD20},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD20},{IMDD5})")
    prolog.assertz(f"has_MDD_intervention({MDD21},{IMDD0})")
    prolog.assertz(f"has_MDD_intervention({MDD22},{IMDD0})")

    prolog.assertz(
        r"all_MDD_intervention_options(I,[S|TAIL]) :- has_MDD_intervention(S, I) ; all_MDD_intervention_options(I,TAIL)"
    )

    # Premenstrual Dysphoric Disorder #
    prolog.assertz(f"has_PREDD_intervention({PREDD0},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD0},{IPREDD2})")
    prolog.assertz(f"has_PREDD_intervention({PREDD0},{IPREDD3})")
    prolog.assertz(f"has_PREDD_intervention({PREDD0},{IPREDD5})")
    prolog.assertz(f"has_PREDD_intervention({PREDD0},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD1},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD1},{IPREDD1})")
    prolog.assertz(f"has_PREDD_intervention({PREDD2},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD2},{IPREDD1})")
    prolog.assertz(f"has_PREDD_intervention({PREDD3},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD4},{IPREDD2})")
    prolog.assertz(f"has_PREDD_intervention({PREDD4},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD5},{IPREDD2})")
    prolog.assertz(f"has_PREDD_intervention({PREDD5},{IPREDD3})")
    prolog.assertz(f"has_PREDD_intervention({PREDD5},{IPREDD5})")
    prolog.assertz(f"has_PREDD_intervention({PREDD5},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD6},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD6},{IPREDD1})")
    prolog.assertz(f"has_PREDD_intervention({PREDD6},{IPREDD4})")
    prolog.assertz(f"has_PREDD_intervention({PREDD7},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD7},{IPREDD1})")
    prolog.assertz(f"has_PREDD_intervention({PREDD7},{IPREDD4})")
    prolog.assertz(f"has_PREDD_intervention({PREDD7},{IPREDD5})")
    prolog.assertz(f"has_PREDD_intervention({PREDD8},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD8},{IPREDD1})")
    prolog.assertz(f"has_PREDD_intervention({PREDD8},{IPREDD4})")
    prolog.assertz(f"has_PREDD_intervention({PREDD9},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD9},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD10},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD10},{IPREDD3})")
    prolog.assertz(f"has_PREDD_intervention({PREDD11},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD11},{IPREDD7})")
    prolog.assertz(f"has_PREDD_intervention({PREDD12},{IPREDD2})")
    prolog.assertz(f"has_PREDD_intervention({PREDD12},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD13},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD14},{IPREDD5})")
    prolog.assertz(f"has_PREDD_intervention({PREDD15},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD15},{IPREDD2})")
    prolog.assertz(f"has_PREDD_intervention({PREDD16},{IPREDD1})")
    prolog.assertz(f"has_PREDD_intervention({PREDD17},{IPREDD2})")
    prolog.assertz(f"has_PREDD_intervention({PREDD18},{IPREDD5})")
    prolog.assertz(f"has_PREDD_intervention({PREDD18},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD19},{IPREDD5})")
    prolog.assertz(f"has_PREDD_intervention({PREDD19},{IPREDD6})")
    prolog.assertz(f"has_PREDD_intervention({PREDD20},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD20},{IPREDD5})")
    prolog.assertz(f"has_PREDD_intervention({PREDD21},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD21},{IPREDD5})")
    prolog.assertz(f"has_PREDD_intervention({PREDD22},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD22},{IPREDD1})")
    prolog.assertz(f"has_PREDD_intervention({PREDD23},{IPREDD0})")
    prolog.assertz(f"has_PREDD_intervention({PREDD23},{IPREDD5})")

    prolog.assertz(
        r"all_PREDD_intervention_options(I,[S|TAIL]) :- has_PREDD_intervention(S, I) ; all_PREDD_intervention_options(I,TAIL)"
    )

    prolog.assertz(f"has_SR_intervention({SR0},{ISR0})")
    prolog.assertz(f"has_SR_intervention({SR0},{ISR1})")
    prolog.assertz(f"has_SR_intervention({SR1},{ISR0})")
    prolog.assertz(f"has_SR_intervention({SR2},{ISR0})")
    prolog.assertz(f"has_SR_intervention({SR2},{ISR1})")
    prolog.assertz(f"has_SR_intervention({SR3},{ISR2})")
    prolog.assertz(f"has_SR_intervention({SR4},{ISR2})")
    prolog.assertz(f"has_SR_intervention({SR5},{ISR0})")
    prolog.assertz(f"has_SR_intervention({SR5},{ISR2})")
    prolog.assertz(f"has_SR_intervention({SR6},{ISR0})")
    prolog.assertz(f"has_SR_intervention({SR7},{ISR0})")

    prolog.assertz(
        r"all_SR_intervention_options(I,[S|TAIL]) :- has_SR_intervention(S, I) ; all_SR_intervention_options(I,TAIL)"
    )


def init_reasoner():
    reasoner = Prolog()
    load_knowledge(reasoner)
    return reasoner


def query_SEAD_intervention_options(reasoner, symptoms):
    ls = symptoms_to_prolog_list(symptoms)
    result = list(reasoner.query(f"all_SEAD_intervention_options(I, {ls})"))
    options = set([])

    for r in result:
        for (_, t) in r.items():
            options.add(t)

    return options


def query_SOAD_intervention_options(reasoner, symptoms):
    ls = symptoms_to_prolog_list(symptoms)
    result = list(reasoner.query(f"all_SOAD_intervention_options(I, {ls})"))
    options = set([])

    for r in result:
        for (_, t) in r.items():
            options.add(t)

    return options


def query_PD_intervention_options(reasoner, symptoms):
    ls = symptoms_to_prolog_list(symptoms)
    result = list(reasoner.query(f"all_PD_intervention_options(I, {ls})"))
    options = set([])

    for r in result:
        for (_, t) in r.items():
            options.add(t)

    return options


def query_GAW_intervention_options(reasoner, symptoms):
    ls = symptoms_to_prolog_list(symptoms)
    result = list(reasoner.query(f"all_GAW_intervention_options(I, {ls})"))
    options = set([])

    for r in result:
        for (_, t) in r.items():
            options.add(t)

    return options


def query_MDD_intervention_options(reasoner, symptoms):
    ls = symptoms_to_prolog_list(symptoms)
    result = list(reasoner.query(f"all_MDD_intervention_options(I, {ls})"))
    options = set([])

    for r in result:
        for (_, t) in r.items():
            options.add(t)

    return options


def query_PREDD_intervention_options(reasoner, symptoms):
    ls = symptoms_to_prolog_list(symptoms)
    result = list(reasoner.query(f"all_PREDD_intervention_options(I, {ls})"))
    options = set([])

    for r in result:
        for (_, t) in r.items():
            options.add(t)

    return options


def query_SR_intervention_options(reasoner, symptoms):
    ls = symptoms_to_prolog_list(symptoms)
    result = list(reasoner.query(f"all_SR_intervention_options(I, {ls})"))
    options = set([])

    for r in result:
        for (_, t) in r.items():
            options.add(t)

    return options


def symptoms_to_prolog_list(symptoms):
    return "[" + ",".join(map(lambda s: str(s), symptoms)) + "]"
