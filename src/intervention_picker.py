import numpy as np
from skfuzzy import control as ctrl


def generate_model():
    # Antecedents
    gender = ctrl.Antecedent(np.arange(0, 1.1, 0.1), "gender")

    male_risk = ctrl.Antecedent(np.arange(0, 1.01, 0.01), "male_risk")
    female_risk = ctrl.Antecedent(np.arange(0, 1.01, 0.01), "female_risk")

    current_mood = ctrl.Antecedent(np.arange(0, 1.01, 0.01), "current_mood")
    disorder_weight = ctrl.Antecedent(np.arange(0, 1.01, 0.01), "disorder_weight")
    intervention_helpfulness = ctrl.Antecedent(
        np.arange(0, 1.01, 0.01), "intervention_helpfulness"
    )

    # Consequent
    selection_strength = ctrl.Consequent(np.arange(0, 1.01, 0.01), "selection_strength")

    # Membership functions
    gender["male"] = [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
    gender["female"] = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1]

    male_risk.automf(names=["low", "medium", "high"])
    female_risk.automf(names=["low", "medium", "high"])

    current_mood.automf(names=["very_sad", "sad", "normal", "happy", "very_happy"])
    disorder_weight.automf(names=["very_low", "low", "medium", "high", "very_high"])
    intervention_helpfulness.automf(names=["useless", "somewhat_helpful", "helpful"])

    selection_strength.automf(names=["very_low", "low", "medium", "high", "very_high"])

    rule1 = ctrl.Rule(
        disorder_weight["very_low"],
        selection_strength["very_low"],
    )

    rule2 = ctrl.Rule(
        (disorder_weight["very_low"] | disorder_weight["low"])
        & (current_mood["very_sad"] | current_mood["sad"])
        & (
            intervention_helpfulness["somewhat_helpful"]
            | intervention_helpfulness["helpful"]
        ),
        selection_strength["medium"],
    )

    rule3 = ctrl.Rule(
        (
            (disorder_weight["medium"] & male_risk["high"] & gender["male"])
            & (current_mood["very_sad"] | current_mood["sad"])
        )
        & (
            intervention_helpfulness["somewhat_helpful"]
            | intervention_helpfulness["helpful"]
        ),
        selection_strength["high"],
    )

    rule4 = ctrl.Rule(
        (
            (disorder_weight["medium"] & female_risk["high"] & gender["female"])
            & (current_mood["very_sad"] | current_mood["sad"])
        )
        & (
            intervention_helpfulness["somewhat_helpful"]
            | intervention_helpfulness["helpful"]
        ),
        selection_strength["high"],
    )

    rule5 = ctrl.Rule(
        female_risk["high"] & gender["male"],
        selection_strength["low"],
    )

    rule6 = ctrl.Rule(
        male_risk["high"] & gender["female"],
        selection_strength["low"],
    )

    rule7 = ctrl.Rule(
        (disorder_weight["high"] | disorder_weight["very_high"])
        & female_risk["high"]
        & gender["female"]
        & intervention_helpfulness["somewhat_helpful"],
        selection_strength["high"],
    )

    rule8 = ctrl.Rule(
        (disorder_weight["high"] | disorder_weight["very_high"])
        & female_risk["high"]
        & gender["female"]
        & intervention_helpfulness["helpful"],
        selection_strength["very_high"],
    )

    rule9 = ctrl.Rule(
        (disorder_weight["high"] | disorder_weight["very_high"])
        & male_risk["high"]
        & gender["male"]
        & intervention_helpfulness["somewhat_helpful"],
        selection_strength["high"],
    )

    rule10 = ctrl.Rule(
        (disorder_weight["high"] | disorder_weight["very_high"])
        & male_risk["high"]
        & gender["male"]
        & intervention_helpfulness["helpful"],
        selection_strength["very_high"],
    )

    rule11 = ctrl.Rule(
        (current_mood["happy"] | current_mood["very_happy"])
        & (disorder_weight["high"] | disorder_weight["very_high"])
        & (
            intervention_helpfulness["somewhat_helpful"]
            | intervention_helpfulness["useless"]
        ),
        selection_strength["low"],
    )

    rule12 = ctrl.Rule(
        (current_mood["happy"] | current_mood["very_happy"])
        & (intervention_helpfulness["useless"]),
        selection_strength["very_low"],
    )

    rule13 = ctrl.Rule(
        (current_mood["happy"] | current_mood["very_happy"])
        & (disorder_weight["high"] | disorder_weight["very_high"])
        & (
            intervention_helpfulness["somewhat_helpful"]
            | intervention_helpfulness["helpful"]
        ),
        selection_strength["medium"],
    )

    rule14 = ctrl.Rule(
        (current_mood["sad"] | current_mood["very_sad"])
        & (disorder_weight["high"] | disorder_weight["very_high"])
        & gender["male"]
        & male_risk["high"]
        & (female_risk["medium"] | female_risk["low"])
        & intervention_helpfulness["helpful"],
        selection_strength["very_high"],
    )

    rule15 = ctrl.Rule(
        (current_mood["sad"] | current_mood["very_sad"])
        & (disorder_weight["high"] | disorder_weight["very_high"])
        & gender["female"]
        & female_risk["high"]
        & (male_risk["medium"] | male_risk["low"])
        & intervention_helpfulness["helpful"],
        selection_strength["high"],
    )

    rule16 = ctrl.Rule(
        (disorder_weight["high"] | disorder_weight["very_high"])
        & female_risk["medium"]
        & gender["female"]
        & (
            intervention_helpfulness["somewhat_helpful"]
            | intervention_helpfulness["helpful"]
        ),
        selection_strength["high"],
    )

    rule17 = ctrl.Rule(
        (disorder_weight["high"] | disorder_weight["very_high"])
        & male_risk["medium"]
        & gender["male"]
        & (
            intervention_helpfulness["somewhat_helpful"]
            | intervention_helpfulness["helpful"]
        ),
        selection_strength["very_high"],
    )

    rule18 = ctrl.Rule(
        (current_mood["sad"] | current_mood["very_sad"])
        & intervention_helpfulness["useless"],
        selection_strength["low"],
    )

    selection_probability_ctrl = ctrl.ControlSystem(
        [
            rule1,
            rule2,
            rule3,
            rule4,
            rule5,
            rule6,
            rule7,
            rule8,
            rule9,
            rule10,
            rule11,
            rule12,
            rule13,
            rule14,
            rule15,
            rule16,
            rule17,
            rule18,
        ]
    )

    model = ctrl.ControlSystemSimulation(selection_probability_ctrl)

    return model


MALE = 0
FEMALE = 1


def compute_strength(
    model, gender, male_risk, female_risk, current_mood, disorder_weight, helpfulness
):
    assert gender in [MALE, FEMALE]

    model.input["gender"] = gender
    model.input["male_risk"] = male_risk
    model.input["female_risk"] = female_risk
    model.input["current_mood"] = current_mood
    model.input["disorder_weight"] = disorder_weight
    model.input["intervention_helpfulness"] = helpfulness

    model.compute()

    return model.output["selection_strength"]
