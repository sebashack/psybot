#!/bin/bash

set -xeuf -o pipefail

sudo apt-get install -y tar build-essential lsb-release python3-pip python3-tk
sudo apt -y install swi-prolog

pip3 install black
pip3 install flake8
